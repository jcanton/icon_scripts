import xarray as xr
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import pickle
import shapefile
import pyproj
import geopandas as gpd
import auxMethods as am
import shapely
from shapely.ops import transform as shapely_transform
#from shapely import Polygon, intersection, union_all
from rtree import index
from multiprocessing import Pool

#===============================================================================

# Load grid
#grid  = xr.open_dataset('/store/g142/jcanton/repos/icon_input/grids/ZHcityInner_100m/child_grid_DOM01.nc')
grid  = xr.open_dataset('/store/g142/jcanton/repos/icon_input/grids/ZHcityInner_016m2/child_grid_DOM01.nc')
v_x = np.rad2deg( grid.variables['vlon'].values )
v_y = np.rad2deg( grid.variables['vlat'].values )
tri = mpl.tri.Triangulation(v_x, v_y, triangles=(grid.vertex_of_cell.values-1).T)
tri.cells_of_vertex = np.where(grid.cells_of_vertex.values>0, grid.cells_of_vertex.values-1, -1)
cell_area = float(grid.cell_area.mean().values)
n_triangles = len(tri.triangles)


#===============================================================================
# Load buildings

class Building():
    def __init__(self, poly=None, z_floor=0, z_roofMin=1, z_roofMax=2):
        self.poly      = poly
        self.z_floor   = z_floor
        self.z_roofMin = z_roofMin
        self.z_roofMax = z_roofMax
        self.z_roof    = (z_roofMin+z_roofMax)/2.
        self.height    = self.z_roof-self.z_floor

# database of blockModell
shape_file = shapefile.Reader(
    shp='/store/g142/jcanton/repos/shapefiles/swiss_block_buildings_ZHcity/data/geoz.bauten_blockmodell_2d.shp',
    dbf='/store/g142/jcanton/repos/shapefiles/swiss_block_buildings_ZHcity/data/geoz.bauten_blockmodell_2d.dbf'
    )
crs_2056 = pyproj.CRS.from_epsg(2056) # swiss LV95
crs_4326 = pyproj.CRS.from_epsg(4326) # lat/lon WGS 84
transformer = pyproj.Transformer.from_crs(crs_2056, crs_4326, always_xy=True)

buildings_2d = shape_file.numRecords*[None]
for ir, record in enumerate(shape_file.shapeRecords()):
    buildings_2d[ir] = Building(
        poly = shapely_transform(transformer.transform, shapely.geometry.shape(record.shape)),
        z_floor   = record.record['h_boden'],
        z_roofMin = record.record['h_trauf'],
        z_roofMax = record.record['h_first'],
    )

# Create R-tree index for buildings
building_index = index.Index()
for i, bld in enumerate(buildings_2d):
    building_index.insert(i, bld.poly.bounds)

#===============================================================================
# Check intersections using spatial indexing
#
tri_poly = [shapely.Polygon( np.c_[tri.x[tri.triangles[i]], tri.y[tri.triangles[i]]]) for i in range(n_triangles)]
tri_bbox = [poly.bounds for poly in tri_poly]
tri.area = np.array([poly.area for poly in tri_poly])

def check_intersection(itri):

    intersections = []
    heights = []
    for ibld in building_index.intersection(tri_bbox[itri]):
        bld = buildings_2d[ibld]
        int_poly = shapely.intersection(bld.poly, tri_poly[itri])
        if int_poly.is_empty or int_poly.area == 0:
            continue
        intersections.append(int_poly)
        heights.append(bld.height)

    if intersections:
        int_union = shapely.union_all(intersections)
        int_height = np.sum(np.array(heights)*np.array([int.area for int in intersections]))/int_union.area
        return itri, int_union, int_union.area, int_height
    else:
        return itri, None, 0, 0

# Parallelize the intersection checking
num_processes = 4
# Parallelize the intersection checking using a Pool
print('Starting pool')
with Pool(processes=num_processes) as pool:
    results = pool.map(check_intersection, range(len(tri.triangles)))

# Get results
tri.intersections = np.array( [result[1] for result in results if result[1] is not None] )
tri.covered_area  = np.array( [result[2] for result in results] )
tri.height        = np.array( [result[3] for result in results] )
# Save ratio
tri.coverage_percentage  = tri.covered_area/tri.area

# Save to file
with open('triangles.pkl','wb') as ofile:
    pickle.dump(tri, ofile)


fig = plt.figure(112); plt.clf(); plt.show(block=False)
ax = fig.add_subplot(1,1,1)
am.plotPolygons(ax, tri.intersections, style='line')
am.plotPolygons(ax, [bld.poly for bld in buildings_2d],      style='fill')
#ax.triplot(tri, color='black')
ax.axis('equal')
plt.draw()


#===============================================================================
# load results for postprocessing
with open('triangles_016m2.pkl','rb') as ifile:
    tri = pickle.load(ifile)

# Set threshold value
threshold = 0.5  # For example, using 50% coverage as the threshold
# Create a binary mask indicating whether each triangle is a building or not
building_mask = np.where(tri.coverage_percentage > threshold, 1, 0)

fig = plt.figure(113); plt.clf(); plt.show(block=False)
ax = fig.add_subplot(1,1,1)
im=ax.tripcolor(tri, tri.coverage_percentage, shading='flat')
#am.plotPolygons(ax, buildings_2d, style='line', color='red')
ax.axis('equal')
plt.colorbar(im)
plt.draw()


def binaryDilation(tri, field):
    field2 = np.zeros(len(field))
    n_filled=0
    for itri, neighs in enumerate(tri.neighbors):
        if (field[itri]==0) and (np.sum(field[neighs]) > 1):
            # fill in saw-tooth-edges
            field2[itri] = 0.7
            n_filled+=1
    print('filled: {}'.format(n_filled))
    return field+field2
def binaryErosion(tri, field):
    field2 = np.zeros(len(field))
    n_killed=0
    for itri, neighs in enumerate(tri.neighbors):
        if (field[itri]==1) and (np.sum(field[neighs]) == 0):
            # killing isolated triangles
            field2[itri] = -0.7
            n_killed+=1
    print('killed: {}'.format(n_killed))
    return field+field2
def binaryErosion1(tri, field):
    field2 = np.zeros(len(field))
    n_killed=0
    for itri, neighs in enumerate(tri.neighbors):
        if (field[itri]==1) and (np.sum(field[neighs]) == 1):
            # killing triangles with just one friend
            field2[itri] = -0.7
            n_killed+=1
    print('killed: {}'.format(n_killed))
    return field+field2

def regularizeTri(tri, field):
    field2 = np.where(binaryDilation(tri, field) >0.5, 1, 0)
    field3 = np.where(binaryErosion (tri, field2)>0.5, 1, 0)
    field4 = np.where(binaryErosion1(tri, field3)>0.5, 1, 0)
    return field4

building_mask2 = binaryErosion (tri,          building_mask)
building_mask3 = binaryDilation(tri, np.where(building_mask2>0.5,1,0))
building_mask4 = binaryErosion1(tri, np.where(building_mask3>0.5,1,0))

fig = plt.figure(114); plt.clf(); plt.show(block=False)
fig, ax = plt.subplots(nrows=1, ncols=4, num=fig.number, sharex=True, sharey=True)
ax[0].tripcolor(tri, building_mask,  shading='flat', cmap='gist_ncar')
ax[1].tripcolor(tri, building_mask2, shading='flat', cmap='gist_ncar')
ax[2].tripcolor(tri, building_mask3, shading='flat', cmap='gist_ncar')
ax[3].tripcolor(tri, building_mask4, shading='flat', cmap='gist_ncar')
ax[0].set_aspect('equal')
ax[1].set_aspect('equal')
ax[2].set_aspect('equal')
ax[3].set_aspect('equal')
plt.draw()

building_mask2 = np.where(building_mask2>0.5, 1, 0)
building_mask3 = np.where(building_mask3>0.5, 1, 0)
building_mask4 = np.where(building_mask4>0.5, 1, 0)

area0 = cell_area*np.sum(building_mask) 
area1 = cell_area*np.sum(building_mask2)
area2 = cell_area*np.sum(building_mask3)
area3 = cell_area*np.sum(building_mask4)

print('Area 0: {:.2f} m2 | change: {:.2f} %'.format(area0, area0/area0*100))
print('Area 1: {:.2f} m2 | change: {:.2f} %'.format(area1, area1/area0*100))
print('Area 2: {:.2f} m2 | change: {:.2f} %'.format(area2, area2/area0*100))
print('Area 3: {:.2f} m2 | change: {:.2f} %'.format(area3, area3/area0*100))
