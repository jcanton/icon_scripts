#===============================================================================
from icon_uxarray import icon_grid_2_ugrid

#iconGrid_fname = '/store/g142/jcanton/repos/icon_input/grids/ZHcityInner_100m/child_grid_DOM01.nc'
#iconGrid_fname = '/store/g142/jcanton/repos/icon_input/grids/ZHcityInner_010m/child_grid_DOM01.nc'
#iconGrid_fname = '/store/g142/jcanton/repos/icon_input/grids/ZHcityInner_005m/child_grid_DOM01.nc'
#iconGrid_fname = '/store/g142/jcanton/repos/icon_input/grids/ZHcityInner_016m2/child_grid_DOM01.nc'

#uGrid_fname = icon_grid_2_ugrid(iconGrid_fname)


# plot
import uxarray as ux
import panel as pn

#uGrid_fname = '/Users/jcanton/sshfsvolumes/g142/jcanton/repos/icon_input/grids/ZHcityInner_100m/ux_child_grid_DOM01.nc'
#uData_fname = '/Users/jcanton/sshfsvolumes/g142/jcanton/repos/icon_scripts/ux_buildings_100m.nc'
uGrid_fname = '/Users/jcanton/sshfsvolumes/g142/jcanton/repos/icon_input/grids/ZHcityInner_016m2/ux_child_grid_DOM01.nc'
uData_fname = '/Users/jcanton/sshfsvolumes/g142/jcanton/repos/icon_scripts/ux_buildings_016m2.nc'

ux_data = ux.open_dataset(uGrid_fname, uData_fname)
#hvplot = ux_data['building_area_ratio'].plot.polygons()
hvplot = ux_data['building_area_ratio'].plot.rasterize(pixel_ratio=16)
hvplot = ux_data['building_height'].plot.rasterize(pixel_ratio=16)
server = pn.panel(hvplot).show()
