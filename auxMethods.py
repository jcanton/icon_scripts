import numpy as np

#===============================================================================
def distance(a, b) -> float:
    return np.sum((np.array(a)-np.array(b))**2)**0.5

#===============================================================================
def computeEdgesLengths(x, y) -> np.array:
    vertices = [(0,1), (1,2), (2,0)]
    edges = np.zeros(len(x))
    for iedge in range(3):
        edges[iedge] = distance(
            (x[vertices[iedge][0]],y[vertices[iedge][0]]),
            (x[vertices[iedge][1]],y[vertices[iedge][1]])
            )
    return edges

#===============================================================================
def isElongatedTriangle(x, y) -> bool:
    edges = computeEdgesLengths(x, y)
    perimeter = np.sum(edges)
    if perimeter > 4*edges.min():
        return True
    else:
        return False

#===============================================================================
def isTorusBorderTriangle(x, y) -> bool:
    edges = computeEdgesLengths(x, y)
    perimeter = np.sum(edges)
    if perimeter > 25:
        return True
    elif perimeter > 4*edges.min():
        return True
    else:
        return False

#===============================================================================
def plotPolygons(ax, polygons, style='fill', color=None) -> None:
    #
    for poly in polygons:
        if poly is not None:
            if poly.geom_type == 'Polygon':
                x, y = poly.exterior.xy
                if style == 'line':
                    ax.plot(x,y, '-', color=color)
                elif style == 'fill':
                    ax.fill(x,y,      color=color)
            elif poly.geom_type == 'MultiPolygon':
                for subpoly in list(poly.geoms):
                    x, y = subpoly.exterior.xy
                    if style == 'line':
                        ax.plot(x,y, '-', color=color)
                    elif style == 'fill':
                        ax.fill(x,y,      color=color)
