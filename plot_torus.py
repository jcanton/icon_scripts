import os
import uxarray as ux
from icon_uxarray import remove_torus_boundaries
import holoviews as hv
import panel as pn

data_dir = '/Users/jcanton/sshfsVolumes/tsa'

grid_fname = os.path.join(data_dir, 'grid-generator/grids/ux_Torus_Triangles_50000m_x_5000m_res500m.nc')

data_fname = os.path.join(data_dir, 'icon-exclaim/dsl/icon-exclaim/build_torus/experiments/torus_exclaim/torus_exclaim_insta_DOM01_ML_0001.nc')
data_fname = os.path.join(data_dir, 'icon-exclaim/dsl/icon-exclaim/build_torus/experiments/bak.torus_exclaim/torus_exclaim_insta_DOM01_ML_0001.nc')


#===============================================================================
grid = ux.open_grid(grid_fname)

hvplot = (
    grid.plot.edges(color="Black", line_dash='dashed', line_width=0.5) * \
    grid.plot.node_coords(color = "Red", size=4) * \
    grid.plot.face_coords(color = "Blue",  size=4) * \
    grid.plot.edge_coords(color = "Green",  size=4)
) #.opts(title="Node, Edge, & Face Coordinates", xlim=(-150,-90), ylim=(20, 50), width=700, height=350)
server = pn.panel(hvplot).show()


grid2 = remove_torus_boundaries(grid)

hvplot = (
    grid2.plot.edges(color="Black", line_dash='dashed', line_width=0.5) * \
    grid2.plot.node_coords(color = "Red", size=4) * \
    grid2.plot.face_coords(color = "Blue",  size=4) * \
    grid2.plot.edge_coords(color = "Green",  size=4)
) #.opts(title="Node, Edge, & Face Coordinates", xlim=(-150,-90), ylim=(20, 50), width=700, height=350)
server = pn.panel(hvplot).show()

uxds = ux.open_dataset(grid_fname, data_fname)
uxds2 = remove_torus_boundaries(uxds)

# interactive plot
def torus_time_plot(itime, iheight):
    return uxds2['u'].isel(time=itime).isel(height=iheight).plot()
torus = hv.DynamicMap(torus_time_plot, kdims=['time', 'height'])
hvplot = torus.redim.range(time=(0, len(uxds2.time)), height=(0, len(uxds2.height)))
server = pn.panel(hvplot).show()


# hvplot = uxds2['temp'].isel(time=0).isel(height=10).plot().opts(
#     title="Some title",
#     width=1000, height=700,
#     #fontsize={'title': 16, 'labels': 14, 'xticks': 6, 'yticks': 12},
#     #fontscale=1,
#     #xlabel='Custom x-label',
#     #ylabel='Custom y-label',
#     #xlim=(-10, 110), ylim=(-14, 6),
#     #aspect='equal',
#     )
# server = pn.panel(hvplot).show()