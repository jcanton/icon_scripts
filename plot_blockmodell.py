import numpy as np
import pickle
from pyevtk.hl import unstructuredGridToVTK
from pyevtk.vtk import VtkTriangle

#===============================================================================
# Load
#
#fname = 'triangles_100m.pkl'
fname = 'triangles_016m2.pkl'
with open(fname,'rb') as ifile:
    tri = pickle.load(ifile)

n_vertices = len(tri.x)
n_triangles = len(tri.triangles)


#===============================================================================
# Save vtk
#

unstructuredGridToVTK(
    'buildings',
    tri.x*110e3, tri.y*110e3, np.zeros_like(tri.x),
    connectivity=tri.triangles.flatten(),
    offsets=np.arange(3,(n_triangles+1)*3,3),
    cell_types=VtkTriangle.tid*np.ones(n_triangles),
    cellData={'height': tri.height},
    )