import xarray as xr
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import auxMethods as am


#===============================================================================
# Load stuff
grid  = xr.open_dataset('/store/g142/jcanton/repos/grid-generator/grids/Torus_Triangles_100m_x_100m_res5m.nc')

v_x = grid.variables['cartesian_x_vertices'         ].values
v_y = grid.variables['cartesian_y_vertices'         ].values
c_x = grid.variables['cell_circumcenter_cartesian_x'].values
c_y = grid.variables['cell_circumcenter_cartesian_y'].values

tri = mpl.tri.Triangulation(v_x, v_y, triangles=(grid.vertex_of_cell.values-1).T)
n_triangles = len(tri.triangles)

# get periodic border triangles and mask them out
mask = n_triangles*[False]
for itri, idxs in enumerate(tri.triangles):
    if am.isTorusBorderTriangle(tri.x[idxs], tri.y[idxs]):
        mask[itri] = True
tri.set_mask(mask)


# Test plot mesh and triangulation
fig = plt.figure(111); plt.clf(); plt.show(block=False)
ax = fig.add_subplot(1,1,1)
ax.triplot(tri,            color='black')
ax.scatter(v_x, v_y, s=10, color='blue')
ax.scatter(c_x, c_y, s=3,  color='red')
#ax.axis('equal')
plt.draw()

#===============================================================================
# Shapes
#
from shapely import Polygon, intersection

# What to do if shapes are on top of each other as it looks like from the
# buildings database? Preprocess them and cleanup?

polygons = [
        Polygon(((20,21),(40,21),(40,38),(20,38))),
        Polygon(((65,25),(83,25),(84,45),(65,45))),
        Polygon(((60,70),(80,70),(80,85),(60,85))),
        ]
for poly in polygons:
    x,y = poly.exterior.xy
    ax.plot(x,y, '-', color='red')
plt.draw()

tri.areas = np.zeros(n_triangles)
for itri, idxs in enumerate(tri.triangles):
    if tri.mask[itri]:
        continue
    vx = tri.x[idxs]; vy = tri.y[idxs]
    tri_poly = Polygon(( (vx[0], vy[0]), (vx[1], vy[1]), (vx[2], vy[2])))

    for poly in polygons:
        int_poly = intersection(poly, tri_poly)
        area = int_poly.area
        tri.areas[itri] += area
        if area > 0:
            ax.fill(*int_poly.exterior.xy)
            ax.scatter(c_x[itri], c_y[itri], s=20, color='black')
            #ax.text(c_x[itri], c_y[itri], '{:.2f}'.format(area))
            #print('Int area = {}'.format(area))
plt.draw()
