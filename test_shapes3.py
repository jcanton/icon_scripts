import xarray as xr
import uxarray as ux
import shapefile
import pyproj
import numpy as np
import pickle
import auxMethods as am
import shapely
from shapely.ops import transform as shapely_transform
from rtree import index
from multiprocessing import Pool

#===============================================================================
# Load UX grid
#
iconGrid_fname = '/store/g142/jcanton/repos/icon_input/grids/ZHcityInner_100m/ux_child_grid_DOM01.nc'
#iconGrid_fname =  '/store/g142/jcanton/repos/icon_input/grids/ZHcityInner_016m2/ux_child_grid_DOM01.nc'

grid  = ux.open_grid(iconGrid_fname)
n_cells = grid.n_face


#===============================================================================
# Load buildings
#
class Building():
    def __init__(self, poly=None, z_floor=0, z_roofMin=1, z_roofMax=2):
        self.poly      = poly
        self.z_floor   = z_floor
        self.z_roofMin = z_roofMin
        self.z_roofMax = z_roofMax
        self.z_roof    = (z_roofMin+z_roofMax)/2.
        self.height    = self.z_roof-self.z_floor

# database of blockModell
shape_file = shapefile.Reader(
    shp='/store/g142/jcanton/repos/shapefiles/swiss_block_buildings_ZHcity/data/geoz.bauten_blockmodell_2d.shp',
    dbf='/store/g142/jcanton/repos/shapefiles/swiss_block_buildings_ZHcity/data/geoz.bauten_blockmodell_2d.dbf'
    )
crs_2056 = pyproj.CRS.from_epsg(2056) # swiss LV95
crs_4326 = pyproj.CRS.from_epsg(4326) # lat/lon WGS 84 *in degrees*
transformer = pyproj.Transformer.from_crs(crs_2056, crs_4326, always_xy=True)

buildings_2d = shape_file.numRecords*[None]
for ir, record in enumerate(shape_file.shapeRecords()):
    buildings_2d[ir] = Building(
        poly = shapely_transform(transformer.transform, shapely.geometry.shape(record.shape)),
        z_floor   = record.record['h_boden'],
        z_roofMin = record.record['h_trauf'],
        z_roofMax = record.record['h_first'],
    )

# Create R-tree index for buildings
building_index = index.Index()
for i, bld in enumerate(buildings_2d):
    building_index.insert(i, bld.poly.bounds)

#===============================================================================
# Check intersections using spatial indexing
#
deg_node_lon = np.rad2deg(grid.node_lon.data)
deg_node_lat = np.rad2deg(grid.node_lat.data)
face_node_connectivity = grid.face_node_connectivity.data
cells_poly = n_cells*[shapely.Polygon()]
cells_bbox = n_cells*[None]
cells_area = np.zeros(n_cells)
for i in range(n_cells):
    poly = shapely.Polygon( np.c_[deg_node_lon[face_node_connectivity[i]], deg_node_lat[face_node_connectivity[i]]])
    cells_poly[i] = poly
    cells_bbox[i] = poly.bounds
    cells_area[i] = poly.area

def check_intersection(icell):

    intersections = []
    heights = []
    for ibld in building_index.intersection(cells_bbox[icell]):
        bld = buildings_2d[ibld]
        int_poly = shapely.intersection(bld.poly, cells_poly[icell])
        if int_poly.is_empty or int_poly.area == 0:
            continue
        intersections.append(int_poly)
        heights.append(bld.height)

    if intersections:
        int_union = shapely.union_all(intersections)
        int_height = np.sum( np.array(heights)*np.array([int.area for int in intersections]) ) / cells_area[icell]
        return icell, int_union, int_union.area, int_height
    else:
        return icell, None, 0, 0

# Parallelize the intersection checking
num_processes = 4
# Parallelize the intersection checking using a Pool
print('Starting pool')
with Pool(processes=num_processes) as pool:
    results = pool.map(check_intersection, range(n_cells))

# Get results
cells_intersections = np.array( [result[1] for result in results if result[1] is not None] )
cells_covered_area  = np.array( [result[2] for result in results] )
cells_height        = np.array( [result[3] for result in results] )

# Compute ratio
cells_coverage_ratio = cells_covered_area / cells_area

# Save to dataset
ux_ds = ux.UxDataset(
    uxgrid=grid,
    data_vars={
        'building_area_ratio': xr.DataArray(
            data  = cells_coverage_ratio,
            dims  = ['n_face'],
            attrs = {
                'standard_name': 'building_area_ratio',
                'long_name'    : 'ratio of cell area covered by buildings',
                'units'        : '-',
                '_FillValue'   : -1,
            },
        ),
        'building_height': xr.DataArray(
            data  = cells_height,
            dims  = ['n_face'],
            attrs = {
                'standard_name': 'building_height',
                'long_name'    : 'height of buildings',
                'units'        : 'm',
                '_FillValue'   : -1,
            },
        ),
    },
    attrs={ 'title': 'Building Data' },
)
# Save to file
ux_ds.to_netcdf('ux_buildings.nc', 'w', 'NETCDF4')

import panel as pn
hvplot = ux_ds['building_area_ratio'].plot.rasterize(pixel_ratio=16)
hvplot = ux_ds['building_height'].plot.rasterize(pixel_ratio=16)
server = pn.panel(hvplot).show()