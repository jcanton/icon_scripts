import xarray as xr
import numpy as np
from scipy.spatial import Delaunay
import plotly.graph_objects as go


#===============================================================================
# Load stuff
#grid  = xr.open_dataset('/store/g142/jcanton/repos/grid-generator/grids/Torus_Triangles_100m_x_100m_res5m.nc')
grid  = xr.open_dataset('/Users/jcanton/sshfsVolumes/g142/jcanton/repos/grid-generator/grids/Torus_Triangles_100m_x_100m_res5m.nc')

v_x = grid.variables['cartesian_x_vertices']
v_y = grid.variables['cartesian_y_vertices']

c_x = grid.variables['cell_circumcenter_cartesian_x']
c_y = grid.variables['cell_circumcenter_cartesian_y']

#field = grid.variables['clat']

#===============================================================================
# Plot
#
fig=go.Figure()


#-------------------------------------------------------------------------------
# Grid
#
points2D = np.vstack([v_x,v_y]).T
mesh = Delaunay(points2D)
simplices = mesh.simplices
tri =[]
for s in mesh.simplices:
    tri.extend([(s[i], s[i+1])  for  i in range(2)])

utri = list(set(tri))  #utri is the list of unique triangles
I, J = np.asarray(utri).T
x, y = mesh.points.T
verts = mesh.points

tri_vertices= verts[np.asarray(utri)]
Xe = []
Ye = []
for T in tri_vertices:
    Xe += [T[k%2][0] for k in range(2)]+[ None]
    Ye += [T[k%2][1] for k in range(2)]+[ None]

for zgrid in range(0,30,5):

    Ze = []
    for T in tri_vertices:
        Ze += [zgrid for k in range(2)]+[ None]

    lines = go.Scatter3d(x=Xe, y=Ye, z=Ze,
                         mode='lines',
                         line=dict(color= 'rgb(40,40,40)', width=4)
                         )
    fig.add_trace(lines)
    #
    fig.add_mesh3d(x=v_x, y=v_y, z=zgrid*np.ones_like(v_x), color='rgba(0, 218, 245, 0.35)')
    #vertices = go.Scatter3d(x=v_x, y=v_y, z=zgrid*np.ones_like(v_x), mode="markers", marker_size=3)
    #fig.add_trace(vertices)


#-------------------------------------------------------------------------------
# Buildings
#
nx = ny = 100
nz = 2
x, y, z = np.meshgrid(np.linspace(0,100,nx), np.linspace(0,100,ny), np.linspace(0,20,nz))
buildings = np.zeros((nx,ny,nz))

buildings = np.where( (x>=25) & (x<35) & (y>=25) & (y<35) & (z<50), 1, buildings)
buildings = np.where( (x>=65) & (x<75) & (y>=25) & (y<35) & (z<50), 1, buildings)
buildings = np.where( (x>=25) & (x<35) & (y>=65) & (y<75) & (z<50), 1, buildings)
buildings = np.where( (x>=65) & (x<75) & (y>=65) & (y<75) & (z<50), 1, buildings)

surface = go.Isosurface(
    x=x.flatten(), y=y.flatten(), z=z.flatten(),
    value=buildings.flatten(),
    isomin=0.5,
    isomax=1.0,
    opacity=1.0,
    colorscale='RdBu',
    surface_count=2, # number of isosurfaces, 2 by default: only min and max
    showscale=False, # remove colorbar
    )
fig.add_trace(surface)

#-------------------------------------------------------------------------------
fig.update_layout(
        xaxis=dict(visible=False),
        yaxis=dict(visible=False),
        margin=dict(t=0, l=0, b=0), # tight layout
        scene_xaxis_showticklabels=False,
        scene_yaxis_showticklabels=False,
        scene_zaxis_showticklabels=False,
        scene = dict(
            xaxis = dict(visible=False),
            yaxis = dict(visible=False),
            zaxis =dict(visible=False)
        ),
        )
fig.update_scenes(aspectmode='data')
fig.show()
